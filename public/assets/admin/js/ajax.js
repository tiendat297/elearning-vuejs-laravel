
 // xóa giảng viên
 $(document).on("click", ".delete" , function() {
        let id = $(this).data('id');
        $('.del').click(function(){
            $.ajax({
                url: 'delete_admin/'+id,
                dataType : 'json',
                type: 'get',
                success: function (data) {
                   console.log(data);
                   $("#table_giangvien").load(" .data-table");
                   $('#delete').modal('hide');
                },

            });
        });
      });

// xóa khóa học
$(document).on("click", ".xoa" , function() {
    let id = $(this).data('id');
    $('.del').click(function(){
        $.ajax({
            url: 'delete_courses/'+id,
            dataType : 'json',
            type: 'get',
            success: function (data) {
               console.log(data);
               $("#khoahoc").load(" .courses");
               $('#xoa').modal('hide');
            },

        });
    });
  });

  $(document).on("click", ".xoa_nhom" , function() {
    let id = $(this).data('id');
    $('.del').click(function(){
        $.ajax({
            url: 'xoa_group/'+id,
            dataType : 'json',
            type: 'get',
            success: function (data) {
                console.log(data);
                $("#table_giangvien").load(" .data-table");
                $('#delete').modal('hide');
             },

        });
    });
  });

/**-------------------------------------------------------------- */
/**
 * Showtoast
 * view baihoc.blade.php
*/
function showToast ({type = '' , content = '' , title = ''} ) {
        const main = document.getElementById('toast');
        if (main) {
            const toast = document.createElement('div');
            toast.style.animation = `slideLeft ease .5s, fadeOut linear 1s 3s`;
            toast.innerHTML = `
                <div class="alert alert-icon ${type} alert-dismissible fade show bonus " role="alert" id="alert-toast">
                    <div class="alert--icon">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    </div>
                    <div>
                        <i class="mdi mdi-check-all mr-2"></i>
                        <strong>${title} </strong> ${content}
                    </div>
                </div>
            `;
            main.appendChild(toast);

            setTimeout(function() {
                toast.remove();
            }, 4000)
        }
    }
    function showToastSuccess(data) {
        showToast({
            title: 'Thành công',
            type: 'alert-success',
            content: data
        })
    }
    function showToastDanger(data) {
        showToast({
            title: 'Cảnh báo',
            type: 'alert-danger',
            content: data
        })
    }
