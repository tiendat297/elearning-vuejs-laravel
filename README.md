Elearning  là hệ thống bán khóa học và học onl trên nhiều lĩnh vực giống như một số hệ thống lớn như Unica, Udemy, ....
**Các bạn cài đặt trước**
- Composer
- Xampp hoặc Warm Serve
- NodeJs
- Visual Studio Code
**Cách cài đặt**

- _Các bạn đã có git và clone project này về _
`git clone https://gitlab.com/tiendat297/elearning-vuejs-laravel`
`git pull origin elearning-develop`

- _Chạy composer và install những gói đã cài đặt_
`composer install`
`npm install `


- _Tạo database và config database_
`cp .env.example .env`

- _ Cập nhật file env _
DB_CONNECTION=mysql          
DB_HOST=127.0.0.1            
DB_PORT=3306                 
DB_DATABASE=db_elearning
DB_USERNAME=root             
DB_PASSWORD=   

- _Tạo key dự án_
`php artisan key:generate`

- Tạo bảng (ở đây mình đã tận dụng Db của đồ án nên phần này các bạn có thể bỏ qua và import bằng file .sql của m )
`php artisan migrate`

- Cuối cùng thì chạy song song hai dòng lệnh
`npm run watch`
`php artisan serve`


- **Giao diện client: /index**
- **Giao diện admin: /admin**
