<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use Illuminate\Http\Request;
use App\Services\IndexService;
use App\Http\Requests\CourseRequest;
use App\Models\Chapter;
use Mockery\Undefined;

class IndexController extends Controller
{
    protected $IndexService;

    public function __construct(IndexService $IndexService)
    {
        $this->IndexService = $IndexService;

    }
    /**
     * get Courses and Lecture in Index Client
     */
    public function loadCouresAndLecture()
    {
        $couresAndLecture = $this->IndexService->getCourses();
        return response() -> json($couresAndLecture,200);
    }

    /**
     * get Course -> listCourses Admin
     */
    public function loadCourses()
    {
        $courses = Courses::all();
        return response() -> json($courses,200);
    }

    /**
     * Create Course New by admin
     */
    public function insertCourseAdmin(CourseRequest $request, $idCourses)
    {
        if (isset($request->images)) {
            $imageName = $this->uploadImage($request->images);
        } else {
            $imageName = null;
        }

        try {
            if ($idCourses == 'undefined') {
                $idCourses = '';
                $this->IndexService->createCourseByAdmin($request, $imageName, $idCourses);
                return response()->json([
                    'message' => 'Thêm thành công'
                ], 200);
            } else {
                $this->IndexService->createCourseByAdmin($request, $imageName, $idCourses);
                return response()->json([
                    'message' => 'Sửa thành công'
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Upload images
     */
    public function uploadImage($image)
    {
        $imageName = $this->IndexService->uploadImageService($image);
        return $imageName;
    }

    /**
     * Choses and delete Course
     * Can delete all courses
     */
    public function deleteAllCourses(Request $request)
    {
        $idCourses = $request->all();
        $i = $this->IndexService->deleteAllCourseService($idCourses);
        return response()->json([
            'message' => 'Xóa thành công' . $i . 'khóa học',
        ], 200);
    }
    /**
     * get Course and lecture by id
     */

    public function getCourseById($id)
    {
        $course = $this->IndexService->getCourseByServiceId($id);
        return response()->json($course, 200);
    }

    /**
     * Search course by group course or name
     */
    public function searchCourse(Request $request) {

        if($request->idGroupCourse == null) {
            $course = Courses::query()
            ->where('name', 'LIKE', '%' . $request->nameIdCourse . '%')
            ->orWhere('id', 'LIKE', '%' . $request->nameIdCourse . '%')
            ->orWhere('price_sale', 'LIKE', '%' . $request->nameIdCourse . '%')
            ->get();
        } else {
            $course = Courses::query()
            ->where('group_courses_id', 'LIKE', '%' . $request->idGroupCourse . '%') -> get();
        }

        return response()->json($course, 200);
    }

    /**
     * Get learn and Chapter by id Client
     */
    public function getLearnAndChapterById($id) {
        $dataLearnAndChapter = Chapter::query()
        ->where('dtb_chapter.course_id', $id)
        ->with(['learn' => function ($query) {
            $query->orderBy('sort');
        }])->orderBy('sort_chapter')->get();

        return response()->json($dataLearnAndChapter,200);

    }


}
