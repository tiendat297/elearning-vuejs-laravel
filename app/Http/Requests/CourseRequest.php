<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif',
            'video_thuml' => 'required',
            'price_root' => 'numeric',
            'price_sale' => 'numeric',
            'giang_vien_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên khóa học không được bỏ trống',
            'image.mimes' => 'Ảnh không đúng định dạng',
            'name.required' => 'Tên khóa học không được bỏ trống',
            'price_root.numeric' => 'Giá khóa học phải là một số',
            'price_sale.numeric' => 'Giá khóa học phải là một số',
            'giang_vien_id.required' => 'Giảng viên không được bỏ trống',


        ];
    }
}
