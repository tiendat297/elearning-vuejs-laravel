<?php

namespace App\Services;

use App\Models\Courses;
use App\Models\Lectures;
use Illuminate\Http\Request;


class IndexService
{
    public function getCourses()
    {
        // return $courses = Courses::all();
        $lectures = Lectures::limit(5)->get();
        $courses = Courses::with('lecture')->limit(5)->orderBy('id')->get();
        return $coursesAndLecture = [
            'lectures' => $lectures,
            'courses' => $courses
        ];
    }

    /**
     * Get Course by Id => update Course
     */

    public function getCourseByServiceId($id)
    {
       return  Courses::query()->where('id', $id)->with(['lecture'])->first();
    }

    /**
     * Create Course by admin
     */
    public function createCourseByAdmin($request, $imageName, $idCourse)
    {
        if (empty($idCourse)) {
            $course = new Courses();
        } else {
            $course = Courses::find($idCourse);
        }

        if ($imageName != null) {
            $course->images = $imageName;
        }

        $course->name = $request->name;
        $course->description = $request->description;
        $course->free = $request->free;
        $course->giang_vien_id = $request->giang_vien_id;
        $course->search = $request->search;
        $course->status = $request->status;
        $course->video_thuml = $request->video_thuml;
        $course->price_root = $request->price_root;
        $course->price_sale = $request->price_sale;

        $course->save();
    }

    /**
     * upload image
     */
    public function uploadImageService($image)
    {
        $imageName = date('mdHis') . '-' . uniqid() . '.' . $image->getClientOriginalName();
        $image->move('assets/admin/images/courses', $imageName);
        return $imageName;
    }
    /**
     * delete all or multip course
     */
    public function deleteAllCourseService($idCourses)
    {
        $i = 0;
        foreach ($idCourses as $idCourse) {
            $i++;
            $listCourses = Courses::find($idCourse);
            $listCourses->delete();
        }
        return $i;
    }

}
