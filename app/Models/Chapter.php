<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = 'dtb_chapter';

    public function learn()
    {
        return $this->hasMany(Learn::class);
    }
}
