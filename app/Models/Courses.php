<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $table = 'dtb_courses';

    public function lecture()
    {
        return $this->belongsTo(Lectures::class, 'giang_vien_id', 'id');
    }
}
