<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Learn extends Model
{
    protected $table = 'dtb_lesson';

    public function chapter()
    {
        $this->belongsTo(Chapter::class, 'chapter_id', 'id');
    }
}
