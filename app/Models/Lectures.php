<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lectures extends Model
{
    protected $table = 'dtb_lecturers';
    
    public function courses()
    {
        return $this->hasMany(Courses::class);
    }
}
