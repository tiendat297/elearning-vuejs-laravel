import { http } from './http_service';

// export function loadCourses() {
//     return http().get('/index');
// }

export function loadCouresAndLecture() {
    return http().get('/loadCouresAndLecture')
}
export function loadCoures() {
    return http().get('/loadCourses')
}
export function loadLectureEditCourse() {
    return http().get('/loadCouresAndLecture')
}
export function insertCourse(dataCourse, idCourseUpdate) {
    return http().post(`/insertCourseAdmin/${idCourseUpdate}`, dataCourse);
}
export function deleteAllCourses(params) {
    return http().post('/deleteAll', params)
}
export function findCourseById(idCourse) {
    return http().get(`/getCourseById/${idCourse}`)
}
export function searchCourse(formData) {
    return http().post('/searchCourse', formData);
}
export function getLearnAndChapterById(idCourse) {
    return http().get(`/getLearnAndChapterById/${idCourse}`)
}
