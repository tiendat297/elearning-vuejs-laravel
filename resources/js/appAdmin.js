import Vue from "vue";
import App from './App.vue';
import router from './router';

import FlashMessage from '@smartweb/vue-flash-message';
Vue.use(FlashMessage);

const app = new Vue({
    el: '#appAdmin',
    router,
    render: h => h(App)
});
