import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        apiURL: 'http://127.0.0.1:8000/api',
        serverPath: 'http://127.0.0.1:8000',
        message: ''
    },
    getters: {
        MESSAGE: state => {
            return state.message;
        }
    },
    mutations: {
        SET_MESSAGE: (state, payload) => {
            state.message = payload;
        }
    },
    actions: {}
})
