import Vue from 'vue';
import Router from 'vue-router';
import Index from './views/client/index.vue'
import ListCourses from './views/admin/components/listCourses.vue'
import EditCourse from './views/admin/components/editCourses.vue'
import Admin from './views/admin/index.vue'


Vue.use(Router);

const routes = [

    {
        path: '/',
        name: '/',
        component: Index,
        children: [
            {
                path: '/index',
                name: 'index',
                component: () => import('./views/client/home.vue')
            },
            {
                path: '/detail-course/:id',
                name: 'detail-course',
                component: () => import('./views/client/detailCourse.vue')
            },
        ]
    },


    {
        path: '/admin',
        name: '/admin',
        component: Admin,
        children: [
            {
                path: '/adminEdit/',
                name: 'adminEdit',
                component: EditCourse
            },
            {
                path: '/listCourses',
                name: 'listCourses',
                component: ListCourses
            },
            {
                path: '/edit-course/:id',
                name: 'edit-course',
                component: () => import('./views/admin/components/editCourses.vue')
            }
        ],
    },
];

const router = new Router({
    mode: 'history',
    routes: routes,
    linkActiveClass: 'active'
});

export default router;
