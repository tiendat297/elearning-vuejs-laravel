<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <title>Ba Đờn guitar|| Mãi mãi với thời gian</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Responsive bootstrap 4 admin template" name="description">
        <meta content="Coderthemes" name="author">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets\admin\images\favicon.ico">
        <link href="assets\admin\libs\rwd-table\rwd-table.min.css" rel="stylesheet" type="text/css">
        <!-- App css -->
        <script type="text/javascript" src="assets\admin\js\pages\ckd\ckeditor.js"></script>
        <link href="assets\admin\css\bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet">
        <link href="assets\admin\css\icons.min.css" rel="stylesheet" type="text/css">
        <link href="assets\admin\css\app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    </head>
    <body >
        <div id="app"></div>
        <script src="{{ mix('js/app.js') }}"></script>
         <!-- Vendor js -->
        <script src="assets\admin\js\vendor.min.js"></script>

        <!-- google maps api -->

        <!-- gmap js-->
        <script src="assets\admin\libs\gmaps\gmaps.min.js"></script>
        <script src="assets\admin\libs\jquery-tabledit\jquery.tabledit.min.js"></script>
        <script src="assets\admin\libs\toastr\toastr.min.js"></script>

                <script src="assets\admin\js\pages\toastr.init.js"></script>

        <!-- Init js-->
        <script src="assets\admin\libs\katex\katex.min.js"></script>
        <!-- App js -->
        <script src="assets\admin\js\app.min.js"></script>
        <script src="assets\admin\js\ajax.js"></script>
        <!-- Plugins Js -->
        <script src="assets\admin\libs\select2\select2.min.js"></script>
        <script src="assets\admin\libs\bootstrap-touchspin\jquery.bootstrap-touchspin.min.js"></script>
        <!-- Init js-->
        <script src="assets\admin\js\pages\form-advanced.init.js"></script>
        <script src="assets\admin\libs\sweetalert2\sweetalert2.min.js"></script>

        <!-- Sweet alert init js-->
        <script src="assets\admin\js\pages\sweet-alerts.init.js"></script>
        <script src="assets\admin\libs\bootstrap-datepicker\bootstrap-datepicker.min.js"></script>
        <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    </body>
</html>
