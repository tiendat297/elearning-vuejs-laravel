<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <!-- Favicons -->
        <!-- Fontawesome css -->
        <link rel="stylesheet" href="assets\client\css\font-awesome.min.css">
        <!-- Ionicons css -->
        <link rel="stylesheet" href="assets\client\css\ionicons.min.css">
        <!-- linearicons css -->
        <link rel="stylesheet" href="assets\client\css\linearicons.css">
        <!-- Nice select css -->
        <link rel="stylesheet" href="assets\client\css\nice-select.css">
        <!-- Jquery fancybox css -->
        <link rel="stylesheet" href="assets\client\css\jquery.fancybox.css">
        <!-- Jquery ui price slider css -->
        <link rel="stylesheet" href="assets\client\css\jquery-ui.min.css">
        <!-- Meanmenu css -->
        <link rel="stylesheet" href="assets\client\css\meanmenu.min.css">
        <!-- Nivo slider css -->
        <link rel="stylesheet" href="assets\client\css\nivo-slider.css">
        <!-- Owl carousel css -->
        <link rel="stylesheet" href="assets\client\css\owl.carousel.min.css">
        <!-- Bootstrap css -->
        <link rel="stylesheet" href="assets\client\css\bootstrap.min.css">
        <!-- Custom css -->
        <link rel="stylesheet" href="assets\client\css\default.css">
        <!-- Main css -->
        <link rel="stylesheet" href="assets\client\style.css">
        <link rel="stylesheet" href="./assets/fonts/fontawesome-free-5.15.3-web/css/all.min.css">
        <!-- Responsive css -->
        <link rel="stylesheet" href="assets\client\css\responsive.css">
        <link rel="stylesheet" href="assets\client\css\myCss.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <!-- Modernizer js -->
        {{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> --}}
    </head>
    <body >
        <div id="app"></div>
        <script src="{{ mix('js/app.js') }}"></script>
        <!-- jquery 3.2.1 -->
        <script src="assets\client\js\vendor\modernizr-3.5.0.min.js"></script>
        <script src="assets\client\js\vendor\jquery-3.2.1.min.js"></script>
        <!-- Countdown js -->
        <script src="assets\client\js\jquery.countdown.min.js"></script>
        <!-- Mobile menu js -->
        <script src="assets\client\js\jquery.meanmenu.min.js"></script>
        <!-- ScrollUp js -->
        <script src="assets\client\js\jquery.scrollUp.js"></script>
        <!-- Nivo slider js -->
        <script src="assets\client\js\jquery.nivo.slider.js"></script>
        <!-- Fancybox js -->
        <script src="assets\client\js\jquery.fancybox.min.js"></script>
        <!-- Jquery nice select js -->
        <script src="assets\client\js\jquery.nice-select.min.js"></script>
        <!-- Jquery ui price slider js -->
        <script src="assets\client\js\jquery-ui.min.js"></script>
        <!-- Owl carousel -->
        <script src="assets\client\js\owl.carousel.min.js"></script>
        <!-- Bootstrap popper js -->
        <script src="assets\client\js\popper.min.js"></script>
        <!-- Bootstrap js -->
        <script src="assets\client\js\bootstrap.min.js"></script>
        <!-- Plugin js -->
        <script src="assets\client\js\plugins.js"></script>
        <!-- Main activaion js -->
        <script src="assets\client\js\main.js"></script>
        <script src="assets\client\js\myJs.js"></script>
    </body>
</html>
